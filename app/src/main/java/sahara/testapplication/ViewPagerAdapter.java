package sahara.testapplication;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class ViewPagerAdapter extends PagerAdapter {

    Context context;
//    String[] rank;
//    String[] country;
//    String[] population;
//    int[] flag;
    LayoutInflater inflater;
    static int[] arrayvid;
    private VideoView mVideoView;

    public ViewPagerAdapter(Context context, int[] arrayvid) {
        this.context = context;
        this.arrayvid = arrayvid;
    }

//    public ViewPagerAdapter(Context context, String[] rank, String[] country,
//                            String[] population, int[] flag, int[] arrayvid) {
//        this.context = context;
//        this.rank = rank;
//        this.country = country;
//        this.population = population;
//        this.flag = flag;
//        this.arrayvid = arrayvid;
//    }

    @Override
    public int getCount() {
        return arrayvid.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        // Declare Variables
//        TextView txtrank;
//        TextView txtcountry;
//        TextView txtpopulation;
//        ImageView imgflag;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_item, container,
                false);

        // Locate the TextViews in viewpager_item.xml
//        txtrank = (TextView) itemView.findViewById(R.id.rank);
//        txtcountry = (TextView) itemView.findViewById(R.id.country);
//        txtpopulation = (TextView) itemView.findViewById(R.id.population);
//        imgflag = (ImageView) itemView.findViewById(R.id.flag);
        mVideoView = (VideoView) itemView.findViewById(R.id.VVExe);

//        txtrank.setText(rank[position]);
//        txtcountry.setText(country[position]);
//        txtpopulation.setText(population[position]);
//        imgflag.setImageResource(flag[position]);

        mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });

        MediaController mediaController = new MediaController(context, false);
        mediaController.setAnchorView(mVideoView);
        mVideoView.setMediaController(mediaController);
        ((ViewPager) container).addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        ((ViewPager) container).removeView((LinearLayout) object);

    }

    public void pausevideo() {
        mVideoView.stopPlayback();

    }

    public void play(int position) {
        Uri str=Uri.parse("android.resource://sahara.testapplication/"+ arrayvid[position]);
//        mVideoView.setVideoURI(Uri.parse("android.resource://sahara.testapplication/"
//                        + arrayvid[position]));
        mVideoView.setVideoURI(str);

        mVideoView.requestFocus();
        mVideoView.start();

    }
}


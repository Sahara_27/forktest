package sahara.testapplication.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;


import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import sahara.testapplication.R;
import sahara.testapplication.pojo.Note;

public class MyOrderAdapter extends ListAdapter<Note,MyOrderAdapter.ViewHolder> {
    Context context;
    ArrayList<Note> arrayList;

//    public MyOrderAdapter() {
//        super(DIFF_CALLBACK);
//    }

    private static final DiffUtil.ItemCallback<Note> DIFF_CALLBACK = new DiffUtil.ItemCallback<Note>() {
        @Override
        public boolean areItemsTheSame(Note oldItem, Note newItem) {
            return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(Note oldItem, Note newItem) {
            return oldItem.getId().equals(newItem.getId());
        }
    };
    public MyOrderAdapter(Context context, ArrayList<Note> arrayList) {
        super(DIFF_CALLBACK);
        this.context = context;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.listiem,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        holder.txt.setText(String.valueOf(arrayList.get(position).getId()));
    }
    public Note getNoteAt(int position) {
        return getItem(position);
    }
    public void restoreItem(Note item, int position) {
        arrayList.add(position,item);
        notifyItemInserted(position);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txt;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txt=itemView.findViewById(R.id.txt);
        }
    }
}

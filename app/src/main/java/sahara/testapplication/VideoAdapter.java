package sahara.testapplication;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoHolder> {
//    List<VideoList>videoLists;
    Context context;
    ViewPager2 viewPager;
    Handler handler;
    static int[] arrayvid;

    public VideoAdapter(Context context, ViewPager2 viewPager,int[] arrayvid) {
        this.context = context;
        this.viewPager = viewPager;
        this.arrayvid = arrayvid;
    }

    //    public VideoAdapter(List<VideoList> videoLists, Context context, ViewPager2 viewPager) {
//        this.videoLists = videoLists;
//        this.context = context;
//        this.viewPager = viewPager;
//    }



    @NonNull
    @Override
    public VideoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.video_list,parent,false);
        return new VideoHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final VideoHolder holder, final int position) {
//        VideoList videoList=videoLists.get(position);
//        holder.videoView.setVideoPath(videoList.getVideo_url());
//        holder.likecount.setText(videoList.getVideo_like());
//        holder.commentcount.setText(videoList.getVideo_commentes());
//        holder.video_name.setText(videoList.getVideo_name());

        Uri str=Uri.parse("android.resource://sahara.testapplication/"+ arrayvid[position]);
        holder.videoView.setVideoURI(str);

        holder.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
                // final int mytime=mp.getDuration();



                //  new Handler().postDelayed(new Runnable() {
                //  @Override
                //  public void run() {
                //     viewPager.setCurrentItem(position+1,true);
                //     notifyDataSetChanged();
                // }
                // },mytime);


                mp.setLooping(true);
                float videoRatio = mp.getVideoWidth() / (float) mp.getVideoHeight();
                float screenRatio = holder.videoView.getWidth() / (float) holder.videoView.getHeight();
                float scale = videoRatio / screenRatio;
                if(scale >= 1f) {
                    holder.videoView.setScaleX(scale);
                } else {
                    holder.videoView.setScaleY(1f / scale);
                }
            }

        });

    }

    @Override
    public int getItemCount() {
//        return videoLists.size();
        return arrayvid.length;
    }

    public class VideoHolder extends RecyclerView.ViewHolder{
        VideoView videoView;
        TextView likecount,video_name,commentcount;
        ImageView img;
        public VideoHolder(@NonNull View itemView) {
            super(itemView);
            videoView=itemView.findViewById(R.id.videoView);
            img=itemView.findViewById(R.id.myimage);
            likecount=itemView.findViewById(R.id.likecount);
            commentcount=itemView.findViewById(R.id.commentcount);
            video_name=itemView.findViewById(R.id.video_name);


        }
    }
}

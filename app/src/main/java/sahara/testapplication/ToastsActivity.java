package sahara.testapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import sahara.testapplication.Adapter.MyOrderAdapter;
import sahara.testapplication.pojo.Note;

public class ToastsActivity extends AppCompatActivity {

    Button btnCustomToast,btnCustomToast2;
    CommonMethods commonMethods;
    RecyclerView saharacycler;
    String[] str={"1","2","3","4","5","6","1","2","3","4"};
    ArrayList<Note> arrayList=new ArrayList<>();
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toasts);
        commonMethods=new CommonMethods(this);

        linearLayout=findViewById(R.id.linearLayout);
        saharacycler=findViewById(R.id.saharacycler);

        for (int i=0; i<20; i++) {
            Note note = new Note(String.valueOf(i), i);
            arrayList.add(note);
        }

        btnCustomToast=findViewById(R.id.btnCustomToast);
        btnCustomToast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                LayoutInflater inflater = getLayoutInflater();
//                View layout = inflater.inflate(R.layout.custom_toast_layout,findViewById(R.id.toast_layout_root));
                String str="Chaitanya-Android Developer";
//                Toast toast = new Toast(getApplicationContext());
//                toast.setDuration(Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.TOP,0,50);
//                toast.setView(layout);
//                toast.show();
                commonMethods.display_custom_toast(ToastsActivity.this,str);
            }
        });
        btnCustomToast2=findViewById(R.id.btnCustomToast2);
        btnCustomToast2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.custom_login_success,findViewById(R.id.login_success));

                Toast toast = new Toast(getApplicationContext());
                toast.setDuration(Toast.LENGTH_LONG);
//                toast.setGravity(Gravity.CENTER_VERTICAL,0,0);
                toast.setView(layout);
                toast.show();
            }
        });

        MyOrderAdapter adapter=new MyOrderAdapter(this,arrayList);
        LinearLayoutManager layoutManager=new LinearLayoutManager(this,RecyclerView.VERTICAL,false);
        saharacycler.setLayoutManager(layoutManager);
        saharacycler.setAdapter(adapter);

        Snackbar snackbar = Snackbar
                .make(linearLayout, "Item was removed from the list.", Snackbar.LENGTH_LONG);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

                int test = viewHolder.getAdapterPosition();
                Note item = arrayList.get(test);

                Snackbar snackbar = Snackbar
                        .make(linearLayout, "Item was removed from the list.", Snackbar.LENGTH_LONG);

                snackbar.setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//
//                        if(test!=0||test!=arrayList.size()){
                            adapter.restoreItem(item,test);
//                            saharacycler.scrollToPosition(viewHolder.getAdapterPosition());
//                        }


                    }
                });

                snackbar.setActionTextColor(Color.YELLOW);
                snackbar.show();

//                noteViewModel.delete(adapter.getNoteAt(viewHolder.getAdapterPosition()));
//                arrayList.remove(viewHolder.getAdapterPosition());
//                adapter.getNoteAt(viewHolder.getAdapterPosition());
//                adapter.notifyDataSetChanged();
                arrayList.remove(test);
                adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                Toast.makeText(ToastsActivity.this, "Note deleted", Toast.LENGTH_SHORT).show();

            }
        }).attachToRecyclerView(saharacycler);
    }
}
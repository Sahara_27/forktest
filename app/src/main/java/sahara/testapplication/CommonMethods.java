package sahara.testapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.snackbar.Snackbar;
import com.journeyapps.barcodescanner.CompoundBarcodeView;

public class CommonMethods {
    Activity mActivity;


    public CommonMethods(Activity activity) {
        mActivity = activity;
    }

    public void showSettingsAlertforInternetSettings() {

        String provider = "INTERNET";
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                mActivity);

        alertDialog.setTitle(provider + " SETTINGS");

        alertDialog
                .setMessage(provider + " is not enabled! Want to go to settings menu?");

        alertDialog.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_WIFI_SETTINGS);
                        mActivity.startActivity(intent);
                    }
                });

        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

//    public void showSettingsAlert() {
//        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(mActivity);
//
//        //Setting Dialog Title
//        alertDialog.setTitle(R.string.GPSAlertDialogTitle);
//
//        //Setting Dialog Message
//        alertDialog.setMessage(R.string.GPSAlertDialogMessage);
//
//        //On Pressing Setting button
//        alertDialog.setPositiveButton(R.string.action_settings, new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                mActivity.startActivity(intent);
//            }
//        });
//
//        //On pressing cancel button
//        alertDialog.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//
//        alertDialog.show();
//    }

    public boolean isEmail(String sEmail) {

        String email = sEmail.trim();
        email = email.replace(" ", "");

        //String emailPattern = "([*@])";

        if (android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches() && email.length() > 0) {
            return true;
        } else {
            return false;
        }

    }
    public boolean isLocationPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (mActivity.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                System.out.println("Permission is granted");
                return true;
            } else {

                System.out.println("Permission is revoked");
                ActivityCompat.requestPermissions((Activity) mActivity, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                return false;
            }
        } else {
            System.out.println("Permission is granted");
            return true;
        }
    }

    public boolean isSpaceNotValid(String sEmail) {

        return sEmail.matches("^\\s*$");

    }

    public void showerror(String sAnyText) {
        new MaterialDialog.Builder(mActivity)
                .content(sAnyText)
                .positiveText("OK")
                .titleGravity(GravityEnum.CENTER)
                .cancelable(false)

                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();


                    }
                }).show();
    }

    public void showalert(String sAnyText) {
        new MaterialDialog.Builder(mActivity)
                .content(sAnyText)
                .positiveText("cancel")
                .titleGravity(GravityEnum.CENTER)
                .cancelable(false)

                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();



                    }
                }).show();
    }

    public void showalert(String sAnyText,final CompoundBarcodeView barcodeView) {
        new MaterialDialog.Builder(mActivity)
                .content(sAnyText)
                .positiveText("OK")
                .titleColor(mActivity.getResources().getColor(R.color.black))
                .titleGravity(GravityEnum.CENTER)
                .cancelable(false)

                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        barcodeView.resume();
                        dialog.dismiss();



                    }
                }).show();
    }
    public boolean isPasswordlength(String sPassword) {
        sPassword = sPassword.trim();

        return sPassword.length() >= 8;

    }

    public boolean isStringEmpty(String sString) {
        sString = sString.trim();


        return sString.length() >= 2;

    }

    public void movetoactivtywithfinish(Class movetoanotheractivty) {

        Intent moveintent = new Intent(mActivity.getApplicationContext(), movetoanotheractivty);
        mActivity.startActivity(moveintent);
        mActivity.finish();

    }

    public void movetoactivtywithoutfinish(Class movetoanotheractivty) {

        Intent moveintent = new Intent(mActivity.getApplicationContext(), movetoanotheractivty);
        mActivity.startActivity(moveintent);


    }

    public void display_custom_toast(Context context, String str) {
        View view1=LayoutInflater.from(context).inflate(R.layout.custom_toast_layout,null);
        TextView textView=view1.findViewById(R.id.toastTextView);
        textView.setText(str);
        Toast toast = new Toast(context);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP,0,50);
        toast.setView(view1);
        toast.show();
    }

    public String getDeviceID() {
        return Settings.Secure.getString(mActivity.getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public void hideSoftKeyboard() {

        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = mActivity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(mActivity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }



    public void showToast(String sdf) {

        Toast.makeText(mActivity.getApplicationContext(), sdf, Toast.LENGTH_SHORT).show();


    }
    public void showSnackBar(View view,String sdf) {

        Snackbar.make(view, sdf, Snackbar.LENGTH_LONG).show();


    }
    public void showToastt(String sdf) {

        Toast.makeText(mActivity.getApplicationContext(), sdf, Toast.LENGTH_LONG).show();


    }

}

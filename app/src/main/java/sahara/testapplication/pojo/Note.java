package sahara.testapplication.pojo;

public class Note {
    String Id;
    int pos;

    public Note() {
    }

    public Note(String id, int pos) {
        Id = id;
        this.pos = pos;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }
}
